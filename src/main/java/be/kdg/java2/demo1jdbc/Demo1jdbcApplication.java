package be.kdg.java2.demo1jdbc;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.sql.*;

@SpringBootApplication
public class Demo1jdbcApplication implements CommandLineRunner {
    private static final Logger log = LoggerFactory.getLogger(Demo1jdbcApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(Demo1jdbcApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        try {
            Connection connection = DriverManager.getConnection("jdbc:hsqldb:file:dbData/demo", "sa", "");
            Statement statement = connection.createStatement();
            statement.execute("DROP TABLE IF EXISTS PERSONS");
            boolean status = statement.execute("CREATE TABLE PERSONS(ID INTEGER NOT NULL IDENTITY, " +
                    "NAME VARCHAR(100) NOT NULL, FIRSTNAME VARCHAR(100) NOT NULL)");
            log.info("Status:" + status);
            int rowsAffected = statement.executeUpdate("INSERT INTO PERSONS(NAME, FIRSTNAME) " +
                    "VALUES ('JONES', 'JACK'), ('POTTER', 'MIA'), ('REED', 'JACK')");
            log.info("Rows affected:" + rowsAffected);
            String firstname = "JACK' or '1'='1";
            ResultSet resultSet = statement.executeQuery("SELECT * FROM PERSONS WHERE FIRSTNAME = '" + firstname + "'");
           /* PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM PERSONS WHERE FIRSTNAME = ?");
            preparedStatement.setString(1, "JACK");
            ResultSet resultSet = preparedStatement.executeQuery();*/
            while (resultSet.next()) {
                log.info("Found a person:");
                log.info("Id:" + resultSet.getInt("id"));
                log.info("Name:" + resultSet.getString("name"));
                log.info("Firstname:" + resultSet.getString("firstname"));
            }
            resultSet.close();
            statement.close();
            connection.close();
        } catch (SQLException sqlException) {
            log.error(sqlException.getMessage(), sqlException);
        }
    }
}
